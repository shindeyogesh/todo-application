<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h1>Edit Employee</h1>
<form:form method="POST" action="/TODOLIST/editsave" modelAttribute="todoeditmodel">
	<table>
		<tr>
			<td></td>
			<td><form:hidden path="taskid" /></td>
		</tr>
		<tr>
			<td>Task Title :</td>
			<td><form:input path="tasktitle" /></td>
		</tr>
		<tr>
			<td>Task Description :</td>
			<td><form:input path="taskdesc" /></td>
		</tr>
		<tr>
			<td>From Date :</td>
			<td><form:input path="fromdate" type="date" /></td>
		</tr>
		<tr>
			<td>To Date :</td>
			<td><form:input path="todate" type="date" /></td>
		</tr>
		<tr>
		<td>Status :</td>		
			<td><form:select path="Status">
					<option value="Select" selected>Select</option>
					<form:option value="Pending" label="Pending" />
					<form:option value="In Progress" label="In Progress" />
					<form:option value="Completed" label="Completed" />
				</form:select></td> 
		</tr>
		<tr>
		<tr>
			<td></td>
			<td><input type="submit" value="Edit Save" /></td>
		</tr>
	</table>
</form:form>
