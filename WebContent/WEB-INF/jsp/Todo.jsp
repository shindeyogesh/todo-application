<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
.bg {
	background-color: #F2CD91;
	height: 400;
	width: 500;
}

.h {
	height: 40;
	width: 200;
	font-size: 16;
}

.b {
	height: 30;
	width: 90;
	background-color: #661F06;
	color: white;
	font-size: 16;
}
</style>
<h1>Add New TODO Task</h1>
<form:form class="bg" method="post" action="save"
	modelAttribute="todomodel">
	<table>
		<tr>
			<td>Task Title :</td>
			<td><form:input class="h" path="tasktitle" /></td>
		</tr>
		&nbsp &nbsp
		<tr>
			<td>Task Description :</td>
			<td><form:input class="h" path="taskdesc" /></td>
		</tr>
		<tr>
			<td>From Date :</td>
			<td><form:input class="h" path="fromdate" type="text" /></td>
		</tr>
		<tr>
			<td>To Date :</td>
			<td><form:input class="h" path="todate" type="text" /></td>
		</tr>
		<tr>
			<td>Status :</td>


			<td><form:select class="h" path="Status">
					<option value="Select" selected>Select</option>
					<form:option value="Pending" label="Pending" />
					<form:option value="In Progress" label="In Progress" />
					<form:option value="Completed" label="Completed" />
				</form:select></td>
		</tr>
		<tr>

			<td><input class="b" type="submit" value="Save" /></td>
		</tr>
	</table>
</form:form>
