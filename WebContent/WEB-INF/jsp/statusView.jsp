<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
.bg {
	background-color: #F2CD91;
	height: 400;
	width: 500;
}

.h {
	height: 50;
	width: 80;
	background-color: #F2CD91;
	font-size: 16;
}

.b{
height: 30;
width: 100;
background-color: #661F06;
color: white;
font-size: 16;}

.b1{
height: 50;
width: 140;
background-color: #661F06;
color: white;
font-size: 18;
}
</style>
<body>

<table border="2" width="70%" cellpadding="2">
	<tr class="h">
		<th >TASK ID</th>		
		<th>Task Title</th>
		<th>Task Description</th>
		<th>From date</th>
		<th>To date</th>
		<th>Status</th>
		
		<th>Edit</th>
		<th>Delete</th>
	</tr>
	 <c:forEach var="tobean" items="${list}">
		<tr>
			<td>${tobean.taskid}</td>
			<td>${tobean.tasktitle}</td>
			<td>${tobean.taskdesc}</td>
			<td>${tobean.fromdate}</td>
			<td>${tobean.todate}</td>
			<td>${tobean.status}</td> 	 	
			<td><a href="edittodo/${tobean.taskid}">Edit</a></td>
			<td><a href="deletetodo/${tobean.taskid}">Delete</a></td>
		</tr>
	</c:forEach> 
</table>
<br />
<a href="TodoView"><button class="b1">Go to View Page</button></a>

</body>
</html>