<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
.bg {
	background-color: #F2CD91;
	height: 400;
	width: 500;
}

.h {
	height: 40;
	width: 200;
}

.b {
	height: 60;
	width: 120;
	background-color: #661F06;
	color: white;
	font-size: 18;
}
</style>
<body>

	<form class="bg" action="TodoView" method="get">
		<h1>User Login</h1>

		<table>
			<tr>
				<td><b>Email-id</b></td>
				<td><input class="h" type="text" placeholder="Enter Email ID"
					name="email" required></td>
			</tr>

			<tr>
				<td><b>Password</b></td>
				<td><input class="h" type="password"
					placeholder="Enter Password" name="pwd" required></td>
			</tr>

		</table>
		<button class="b" type="submit">Login</button>


		<br> <a href="register">Register page</a>

		<div style="color: red">${error}</div>
	</form>

</body>
</html>