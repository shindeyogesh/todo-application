package com.javatpoint.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.javatpoint.beans.TodoBean;

import com.javatpoint.dao.TodoDao;

@Controller
public class TodoController {
	@Autowired
	TodoDao dao;// will inject dao from xml file

	
	@RequestMapping("/Todo")
	public String showform(Model m) {
		TodoBean tobean = new TodoBean();
		m.addAttribute("todomodel", tobean);
		return "Todo";
	}

	/*
	 * It saves object into database. The @ModelAttribute puts request data into
	 * model object. You need to mention RequestMethod.POST method because default
	 * request is GET
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("tobean") TodoBean tobean) {
		dao.save(tobean);
		return "redirect:/TodoView";// will redirect to viewemp request mapping
	}

	
	@RequestMapping("/TodoView")
	public String TodoView(Model m) {
		List<TodoBean> list = dao.getTodos();
		m.addAttribute("list", list);
		return "TodoView";
	}

	/*
	 * It displays object data into form for the given taskid. The @PathVariable puts
	 * URL data into variable.
	 */
	@RequestMapping(value = "/edittodo/{taskid}")
	public String edit(@PathVariable int taskid, Model m) {
		TodoBean tobean = dao.gettodotaskid(taskid);
		m.addAttribute("todoeditmodel", tobean);
		return "TodoEdit";
	}

	/* It updates model object. */
	@RequestMapping(value = "/editsave", method = RequestMethod.POST)
	public String editsave(@ModelAttribute("tobean") TodoBean tobean) {
		dao.update(tobean);
		return "redirect:/TodoView";
	}

	/* It deletes record for the given taskid in URL and redirects to /TodoView */
	@RequestMapping(value = "/deletetodo/{taskid}", method = RequestMethod.GET)
	public String delete(@PathVariable int taskid) {
		dao.delete(taskid);
		return "redirect:/TodoView";
	}
	
	
	
}