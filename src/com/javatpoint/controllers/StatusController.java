package com.javatpoint.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.javatpoint.beans.TodoBean;
import com.javatpoint.dao.StatusDao;

@Controller
public class StatusController {
	
	@Autowired
	StatusDao sdao;

	/*@RequestMapping("/Todo")
	public String showform(Model m) {
		TodoBean tobean = new TodoBean();
		m.addAttribute("todomodel", tobean);
		return "Todo";
	}*/
	
	@RequestMapping("/pending")
	public String Todopendingview(Model m) {
		List<TodoBean> liststatus = sdao.getpending();
		m.addAttribute("list", liststatus);
		return "statusView";
	}
	
	@RequestMapping("/completed")
	public String TodoCompleteView(Model m) {
		List<TodoBean> liststatus = sdao.getcompleted();
		m.addAttribute("list", liststatus);
		return "statusView";
	}
	
	@RequestMapping("/inprogress")
	public String TodoinprogressView(Model m) {
		List<TodoBean> liststatus = sdao.getinprogress();
		m.addAttribute("list", liststatus);
		return "statusView";
	}
	
}
