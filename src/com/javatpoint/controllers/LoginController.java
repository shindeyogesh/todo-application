package com.javatpoint.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.javatpoint.beans.Registerbean;
import com.javatpoint.dao.UserDao;

@Controller
public class LoginController {

	UserDao userdao;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String userLogin(@RequestParam("email") String email, @RequestParam("pwd") String pwd, Model m) {
		//int status = 0;
		Registerbean rbean = new Registerbean();
		rbean.setEmail(email);
		rbean.setPwd(pwd);

		String useremail = userdao.loginUser(rbean);

		if (useremail != null) {

			//m.addObject("msg", "Welcome " + useremail + ", You have successfully logged in.");
			m.addAttribute("loginmodel", rbean);
			return "TodoView";

		} else {

			m.addAttribute("msg","Invalid User");
			return "TodoView";
		}
		//return m;
	}

}
