package com.javatpoint.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.javatpoint.beans.Registerbean;
import com.javatpoint.dao.RegisterDao;

@Controller
public class RegController {
	@Autowired
	RegisterDao rdao;
	
	/*@RequestMapping("/register")
	public String showregisetrform(Model m) {
		Registerbean rbean = new Registerbean();
		m.addAttribute("registermodel", rbean);
		return "register";
	}*/
	
	@RequestMapping ("/register")
	public String redirect() {
		
		return "register";	
	}	
	@RequestMapping(value = "/savereg", method = RequestMethod.POST)
	public String savereg(@ModelAttribute("rbean") Registerbean rbean) {
		rdao.registersave(rbean);
		
		return "redirect:/register";
		
	}
	
}
