package com.javatpoint.dao;

import org.springframework.jdbc.core.JdbcTemplate;

import com.javatpoint.beans.Registerbean;

public class UserDao {
	private JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public String loginUser(Registerbean rbean) {
		String sql = "SELECT email FROM gp1userregister WHERE email=? AND pwd=?";

		try {

			String userId = template.queryForObject(sql, new Object[] { rbean.getEmail(), rbean.getPwd() },
					String.class);

			return userId;

		} catch (Exception e) {
			return null;
		}
	}
}
