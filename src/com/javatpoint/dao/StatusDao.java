package com.javatpoint.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.javatpoint.beans.TodoBean;

public class StatusDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}
	
	public TodoBean completed(String status) {
		String sql = "select * from gp1task where Status = 'Completed'";
		return template.queryForObject(sql, new Object[] { status },
				new BeanPropertyRowMapper<TodoBean>(TodoBean.class));
	}
	
	public TodoBean pending(String status) {
		String sql = "select * from gp1task where Status = 'Pending'";
		return template.queryForObject(sql, new Object[] { status },
				new BeanPropertyRowMapper<TodoBean>(TodoBean.class));
	}
	
	public TodoBean inprogress(String status) {
		String sql = "select * from gp1task where Status = 'In Progress'";
		return template.queryForObject(sql, new Object[] { status },
				new BeanPropertyRowMapper<TodoBean>(TodoBean.class));
	}
	
	public List<TodoBean> getcompleted() {
		return template.query("select * from gp1task where status='Completed'", new RowMapper<TodoBean>() {
			public TodoBean mapRow(ResultSet rs, int row) throws SQLException {
				TodoBean todo = new TodoBean();
				todo.setTaskid(rs.getInt(1));
				todo.setTasktitle(rs.getString(2));
				todo.setTaskdesc(rs.getString(3));
				todo.setFromdate(rs.getString(4));
				todo.setTodate(rs.getString(5));
				todo.setStatus(rs.getString(6));
				return todo;
			}
		});
	}
	
	public List<TodoBean> getpending() {
		return template.query("select * from gp1task where status='Pending'", new RowMapper<TodoBean>() {
			public TodoBean mapRow(ResultSet rs, int row) throws SQLException {
				TodoBean todo = new TodoBean();
				todo.setTaskid(rs.getInt(1));
				todo.setTasktitle(rs.getString(2));
				todo.setTaskdesc(rs.getString(3));
				todo.setFromdate(rs.getString(4));
				todo.setTodate(rs.getString(5));
				todo.setStatus(rs.getString(6));
				return todo;
			}
		});
	}
	
	public List<TodoBean> getinprogress() {
		return template.query("select * from gp1task where status='In Progress'", new RowMapper<TodoBean>() {
			public TodoBean mapRow(ResultSet rs, int row) throws SQLException {
				TodoBean todo = new TodoBean();
				todo.setTaskid(rs.getInt(1));
				todo.setTasktitle(rs.getString(2));
				todo.setTaskdesc(rs.getString(3));
				todo.setFromdate(rs.getString(4));
				todo.setTodate(rs.getString(5));
				todo.setStatus(rs.getString(6));
				return todo;
			}
		});
	}	

}
