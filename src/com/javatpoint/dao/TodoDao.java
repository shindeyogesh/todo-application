package com.javatpoint.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;


import com.javatpoint.beans.TodoBean;

public class TodoDao {
	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public int save(TodoBean p) {
		String sql = "insert into gp1task(tasktitle,taskdesc,fromdate,todate,Status)values('" + p.getTasktitle() + "','"
				+ p.getTaskdesc() + "','" + p.getFromdate() + "','" + p.getTodate() + "','" + p.getStatus() + "')";
		return template.update(sql);
	}

	public int update(TodoBean p) {
		String sql = "update gp1task set tasktitle='" + p.getTasktitle() + "', taskdesc='" + p.getTaskdesc()
				+ "',fromdate='" + p.getFromdate() + "', todate ='" + p.getTodate() + "',Status = '" + p.getStatus()
				+ "' where taskid=" + p.getTaskid() + "";
		return template.update(sql);
	}

	public int delete(int taskid) {
		String sql = "delete from gp1task where taskid=" + taskid + "";
		return template.update(sql);
	}

	public TodoBean gettodotaskid(int taskid) {
		String sql = "select * from gp1task where taskid=?";
		return template.queryForObject(sql, new Object[] { taskid },
				new BeanPropertyRowMapper<TodoBean>(TodoBean.class));
	}
	
	
	

	public List<TodoBean> getTodos() {
		return template.query("select * from gp1task ", new RowMapper<TodoBean>() {
			public TodoBean mapRow(ResultSet rs, int row) throws SQLException {
				TodoBean todo = new TodoBean();
				todo.setTaskid(rs.getInt(1));
				todo.setTasktitle(rs.getString(2));
				todo.setTaskdesc(rs.getString(3));
				todo.setFromdate(rs.getString(4));
				todo.setTodate(rs.getString(5));
				todo.setStatus(rs.getString(6));
				return todo;
			}
		});
	}

}